//
//  CustomAlertViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/15/22.
//

import Foundation
import UIKit

class CustomAlertViewController: UIViewController{
    
    
    
    @IBOutlet var AlertView: UIView!
    var parentVC: FlashcardSetDetailViewController!
    @IBOutlet var Termfield: UITextField!
    @IBOutlet var Definitionfield: UITextField!
    
    @IBOutlet var Savebutton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToLookPretty()
        //set text and description of dialog
        Termfield.text = parentVC.sets2[parentVC.selectedIndex].term
        Definitionfield.text = parentVC.sets2[parentVC.selectedIndex].definition
    }
    
    @IBAction func Save(_ sender: Any) {
        //change title/description to the new text and tell the tableView to reload
        parentVC.sets2[parentVC.selectedIndex].term = Termfield.text ?? ""
        parentVC.sets2[parentVC.selectedIndex].definition = Definitionfield.text ?? ""
        parentVC.Tableview.reloadData()
        self.dismiss(animated: true, completion: {})
        
    }
    
    
    func setupToLookPretty()
    {
        AlertView.layer.cornerRadius = 8.0
        AlertView.layer.borderWidth = 3.0
        AlertView.layer.borderColor = UIColor.gray.cgColor
        Termfield.becomeFirstResponder()
    }
    
}

