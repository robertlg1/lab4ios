//
//  FlashcardSetDetilViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/8/22.
//

import Foundation
import UIKit

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var Tableview: UITableView!
    var sets2: [Flashcard] = [Flashcard]()
    var selectedIndex: Int = 0
    
    
    @IBOutlet var Addbutton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let setClass = Flashcard()
        //connect hard coded collection to sets
        sets2 = Flashcard.getHardCodedFlashcards()
        Tableview.delegate = self
        Tableview.dataSource = self
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        self.Tableview.addGestureRecognizer(longPressGesture)    }
    
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        //find out where the long press is
        let p = longPressGesture.location(in: self.Tableview)
        let indexPath = self.Tableview.indexPathForRow(at: p)
        //if long press is starting (this will also trigger on ending if you dont have this if statement)
        if longPressGesture.state == UIGestureRecognizer.State.began {
            selectedIndex = indexPath?.row ?? 0
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let alertVC = sb.instantiateViewController(identifier: "CustomAlertViewController") as! CustomAlertViewController
            alertVC.parentVC = self
            alertVC.modalPresentationStyle = .overCurrentContext
            self.present(alertVC, animated: true, completion: nil)
        }
    }
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return sets2.count
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Mycell", for: indexPath) as! FlashcardTableViewcell
    cell.Termcell.text = sets2[indexPath.row].term
    cell.Definitioncell.text = sets2[indexPath.row].definition
    cell.selectionStyle = .none
    return cell
}
    
    
    @IBAction func ab2(_ sender: Any) {
        
        let fc = Flashcard()
        fc.term = "Term 11"
        fc.definition = "Definition 11"
        sets2.append(fc)
        Tableview.reloadData()
        
    }
    
    func createAlert(indexpath: IndexPath) {
        let alert = UIAlertController(title: "\(sets2[indexpath.row].term)", message:"\(sets2[indexpath.row].definition)" , preferredStyle: .alert)
        selectedIndex = indexpath.row
        alert.addAction(UIAlertAction(title:"Cancel", style: .cancel, handler: {_ in alert.dismiss(animated: true, completion: {})}))
        alert.addAction(UIAlertAction(title:"Edit",style:.default ,handler: {_ in
            alert.dismiss(animated: true, completion: {})
            self.createCustomAlert()
        }))
        self.present(alert, animated: true)
        
    }
    
    func createCustomAlert(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        alertVC.parentVC = self
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: true,completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        createAlert(indexpath: indexPath)
    }
    @IBAction func studybutton(_ sender: Any) {
        
        performSegue(withIdentifier: "GoToStudyView", sender: self)
    }
    

}
