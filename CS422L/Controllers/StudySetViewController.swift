//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/11/22.
//

import UIKit

class StudySetViewController: UIViewController{
    
    var sets3: [Flashcard] = [Flashcard]()
    var missed = Array<Flashcard>()
    var countcorrect = 0
    var countmissed = 0
    var countcomplete = 0
    var num = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        sets3 = Flashcard.getHardCodedFlashcards()
        TermDef.text = sets3[num].term
    }
    

    @IBAction func Switchbutton(_ sender: Any) {
        TermDef.text = sets3[num].definition    }
    
    
    @IBOutlet var TermDef: UILabel!
    

    
    
    @IBOutlet var Completed: UILabel!
    
    
    @IBOutlet var Missnum: UILabel!
    

    @IBOutlet var CorrectNum: UILabel!
    

    @IBOutlet var Missbutton: UIButton!
    
    @IBOutlet var Skipbutton: UIButton!
    
    @IBOutlet var CorrectButton: UIButton!
    
    func back(){
        let sub = sets3[num]
        sets3.remove(at: num)
        sets3.append(sub)
    }

    @IBAction func MissedButton(_ sender: Any) {
        missed.append(sets3[num])
        back()
        TermDef.text = sets3[num].term
        countmissed += 1
        Missnum.text = "Missed: \(countmissed)"
        TermDef.text = sets3[num].term
    }
    
    @IBAction func Skipbtton(_ sender: Any) {
        back()
        TermDef.text = sets3[num].term
        
    }
    
    
    @IBAction func Correctbutton(_ sender: Any) {
        let check = missed.contains(where: { flashcard in
            if flashcard.term == sets3[num].term{
                return true
            }
                else{ return false
                }
        })
        if check {
            
        }
        else{
            countcorrect += 1
            
        }
        sets3.remove(at: num)
        TermDef.text = sets3[num].term
        countcomplete += 1
        Completed.text = "Complete: \(countcomplete)"
        CorrectNum.text = "Correct: \(countcorrect)"
        
    }
    
    
    
    @IBAction func ExitButton(_ sender: Any) {
        performSegue(withIdentifier: "GoBackToDetail", sender: self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
